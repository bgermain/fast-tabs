;
/**
 * Fast Tabs
 *
 * A jquery/Zepto Plugin for building tabs or accordion. It can be build or destroy as needed thus aimed for responsive website use.
 *
 * TODO
 * - work on responsive behaviour of the tabs module
 */

(function($) {
  'use strict';
  var pluginName = 'fastTabs',
    defaults = {
      heading: '.tab-heading',
      body: '.tab-panel',
      activeClass: 'active',
      // responsive: true,
      mode: 'tabs', // possible values: 'tabs', 'toggle' or 'accordion'
      activeHash: false, // true or false, true to use and update hash in url
      start: 0, // possible values: 'none', 'all' or a numerical index starting at 0
      uptop:true,
      toggle: function() {},
      init: function() {},
      debug: false
    };

  /**
   * Plugin Constructor. This function build the basic object for the plugin
   * @param (object) element - The jQuery or Zepto DOM element
   * @param (object) options - A list of options for the plugin
   */
  $[pluginName] = function(element, options) {
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.debug = this.options.debug;

    this.element = element;
    this.$element = $(element);
    this.elementHTML = this.$element.html();

    this.clickTapEvent = 'onTap' in window ? 'tap' : 'click';
    // Launch the tab plugins
    this.initialize();
  };

  // Plugin prototype
  /**
   * The plugin prototype that list all availables methods.
   * @type {Object}
   */
  $[pluginName].prototype = {

    /**
     * initialize the plugin. First it gets all the elements then build the component. When all is done it fires the init option callback
     * @return {[type]}
     */
    initialize: function() {
      this.debug && console.log('initialize in mode ' + this.options.mode, this);

      this.$tabBodys = this.$element.find(this.options.body);
      this.build(this.options.mode)

      typeof this.options.init === 'function' && this.options.init();
      // if (this.options.responsive) {
      //     $(window).resize($.proxy(this.onResize, this)).resize();
      // }
    },

    /**
     * Process the navigation elements then build the component and set the plugin live status.
     * @param  {string} mode - the plugin mode (accordion/tabs/toggle)
     */
    build: function(mode) {
      this.building = true;

      this.$element.addClass(this.options.mode);
      this.tabs = this.processTabs();
      this.$nav = this.createNav();

      var hashElt = window.location.hash ? this.$element.find(window.location.hash) : $(),
        indexToShow = this.options.activeHash && hashElt.length && mode !== 'toggle' ? this.$tabBodys.indexOf(hashElt[0]) : this.options.start;

      mode === 'toggle' || mode === 'accordion' ? this.toggle(indexToShow) : this.activate(indexToShow);
      this.$element.data(pluginName + 'Alive', true);

      this.building = false;
    },

    /**
     * Process all the navigation elements in the DOM and store them in the plugin main object.
     * @return {object} - The tabs label, class name and DOM elements
     */
    processTabs: function() {
      var tabs = [];
      this.count = 0;
      $.each(this.$tabBodys, $.proxy(function(index, tab) {
        var $tab = $(tab);
        $tab.data('heading', $tab.find(this.options.heading));
        $tab.data('index', index);

        tabs[index] = {
          el: $tab,
          heading: $tab.data('heading'),
          title: $tab.data('heading').text(),
          cssClass: typeof $tab.data('heading').attr('class') !== 'undefined' ? $tab.data('heading').attr('class') : '',
          evtClick: typeof $tab.data('heading').attr('onclick') !== 'undefined' ? $tab.data('heading').attr('onclick') : '',
          urlAjax: typeof $tab.data('heading').data('urlajax') !== 'undefined' ? $tab.data('heading').data('urlajax') : ''
        };

        $tab.data('heading').remove();

        this.count++;
      }, this));

      return tabs;
    },

    /**
     * Create the plugin navigation according the option mode.
     * @return {[type]}
     */
    createNav: function() {
      var Constructor = this,
        mode = Constructor.options.mode,
        cases = {
          'accordion': Constructor.createAccordion,
          'tabs': Constructor.createTabs,
          'toggle': Constructor.createAccordion
        };

      if (cases[mode]) {
        return cases[mode].apply(Constructor, arguments)
      }
    },

    /**
     * Build the accordion/toggle navigation elements
     * @return {object} nav - The jQuery/Zepto DOM Element containing the navigation
     */
    createAccordion: function(params) {
      var Constructor = this,
        nav = $();


      $.each(Constructor.tabs, $.proxy(function(index, tab) {
        var $tab = tab.heading,
          $tabBody = Constructor.$tabBodys.eq(index);

        $tabBody.before($tab)
        $tab.on(Constructor.clickTapEvent, $.proxy(function(e) {
          e.preventDefault();
          Constructor.toggle(index);
        }, Constructor));

        nav.push($tab[0]);
      }, Constructor));

      return nav;
    },

    /**
     * Build the tab navigation elements
     * @return {object} nav - The jQuery/Zepto DOM Element containing the navigation
     */
    createTabs: function() {
      var Constructor = this,
        nav = $('<nav></nav>');

      $.each(Constructor.tabs, $.proxy(function(index, tab) {
        nav.append(
          $('<a />')
          .data('index', index)
          .addClass(tab.cssClass)
          .attr("onclick", tab.evtClick)
          .append($('<span />').text(tab.title))
          .on(Constructor.clickTapEvent, $.proxy(function() {
            Constructor.activate(index);
          }, Constructor))
        );
      }, Constructor));
      Constructor.$element.prepend(nav);

      return nav;
    },

    /**
     * Activate the selected tab method
     * @param  {number} tabNr - the selected tab index starting at 0
     */
    activate: function(tabNr) {
      var tab = typeof tabNr !== 'undefined' && tabNr !== '' ? parseInt(tabNr, 10) : 0,
        tabHeading = this.options.heading,
        tabBody = this.options.body,
        activeHash = this.options.activeHash,
        activeClass = this.options.activeClass,
        building = this.building,
        tabs = this.tabs,
        $tabBodys = this.$tabBodys,
        activeTab;

      tab = tab > this.count - 1 ? this.count - 1 : tab;
      tab = tab < 0 ? 0 : tab;

      this.debug && console.log('tab nr {' + tabNr + '} should be activated');

      $.each(this.$nav.find(this.options.heading), function(index) {
        if (index === tab) {
          // $(this).attr(activeClass, true).siblings(this.options.heading).attr(activeClass, false);
          activeTab = $(this)
            .addClass(activeClass)
            .siblings(tabHeading)
            .removeClass(activeClass);
          if (tabs[tab].urlAjax && !$($tabBodys[tab]).data('load')) {
            $.ajax({
              url: tabs[tab].urlAjax,
              dataType: 'html',
              success: function(response) {
                $($tabBodys[tab]).data('load', true);
                $($tabBodys[tab]).empty();
                $($tabBodys[tab]).html(response);
              }
            })
          }
        }
      });

      $.each(this.$tabBodys, function(index) {
        if (index === tab) {
          // $(this).attr(activeClass,true).siblings('tab').attr(activeClass,false);
          $(this)
            .addClass(activeClass)
            .siblings(tabBody)
            .removeClass(activeClass);
          activeHash && !building && this.id && (window.location.hash = this.id);
        }
      });

      if (activeHash && activeTab && !building && this.options.uptop) $(window).scrollTop(activeTab.offset().top);

      if (typeof this.options.toggle === 'function') {
        this.options.toggle(this.$tabBodys[tab]);
      }
    },

    /**
     * Toggle and accordion selecting method
     * @param  {number} index
     */
    toggle: function(index) {
      var Constructor = this,
        tabs = Constructor.tabs,
        $activeTab = typeof index === 'number' ? Constructor.$nav.eq(index) : false,
        $nav = Constructor.$nav,
        $tabs = Constructor.$tabBodys,
        $activeTabBody = typeof index === 'number' ? tabs[index].el : $(),
        activeClass = Constructor.options.activeClass;

      Constructor.debug && console.log('tab nr {' + index + '} should be activated');

      switch (Constructor.options.mode) {
        case 'toggle':
          if (index === 'none') {
            $tabs.removeClass(activeClass);
            $tabs.hide();
          } else if (index === 'all') {
            $tabs.addClass(activeClass);
            $nav.addClass(activeClass);
            $tabs.show();
          } else if (typeof index === 'number') {
            $tabs
              .eq(index)
              .toggle()
              .toggleClass(activeClass);

            $activeTab.toggleClass(activeClass);
          }
          break;
        case 'accordion':
          if (typeof index === 'number') {
            $tabs
              .hide()
              .removeClass(activeClass)
              .eq(index)
              .show()
              .addClass(activeClass);
            $tabs.removeClass(activeClass);
            $nav.removeClass(activeClass);
            $activeTab.addClass(activeClass);
            Constructor.options.activeHash && !Constructor.building && $activeTabBody[0].id && (window.location.hash = $activeTabBody[0].id);
          }
          break;
      }

      // if (typeof index === 'number')
      if ($activeTab && !Constructor.building && Constructor.options.uptop) $(window).scrollTop($activeTab.offset().top);
    },

    // onResize: function() {
    //     clearTimeout(this.timeout);
    //     this.timeout = setTimeout($.proxy(function() {
    //         this.$element.data('break', this.navWidth >= $(window).width());
    //     }, this), 200);
    // },

    /**
     * Destroy the plugin giving you back the original html code of the container
     */
    destroy: function() {
      this.debug && console.log('destroying : ', this);

      this.$element.removeClass(this.options.mode);
      this.$element.html(this.elementHTML);
      this.$element.data(pluginName + 'Alive', false);
      this.$element.removeData(pluginName);
    }
  };

  // Building the plugin
  /**
   * The plugin component
   * @param  {object} options - list of all parameters for the jQuery/Zepto module
   * @return {object} - The jQuery/Zepto DOM element
   */
  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$(this).data(pluginName)) {
        if (options === 'destroy') {
          return;
        }
        $(this).data(pluginName, new $[pluginName](this, options));
      } else {
        var $plugin = $(this).data(pluginName);
        switch (options) {
          case 'destroy':
            $plugin.destroy();
            break;
          default:
            if (typeof options === 'number') {
              $plugin.activate(options);
            }
        }
      }
    });
  };

})(window.Zepto || window.jQuery);