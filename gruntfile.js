module.exports = function(grunt) {

  grunt.initConfig({
    project: {
      name: "fast-tabs"
    },
    uglify: {
      def : {
        files:
          {
            '<%= project.name %>.min.js' : 'src/<%= project.name %>.js'
          }
        }
    },
    cssmin:{
      def :{
        files:
        {
          '<%= project.name %>.min.css' : 'src/<%= project.name %>.css'
        }
      }
    },
    stylus: {
      compile: {
        options: {
          banner: '/* <%= project.name %> styles */',
          use: [
            require('fluidity')
          ]
        },
        files: {
            'src/<%= project.name %>.css' : 'stylus/project.styl'
        }
      }
    },
    watch: {
      def: {
        files: [
          'stylus/*.styl'
        , 'src/*.js'
        ]
      , tasks: [
          'stylus'
        , 'uglify'
        , 'cssmin'
        ]
      , options : {
          spawn : false,
          livereload: {
            port: 9000
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', ['watch:def']);
  grunt.registerTask('js-task', ['uglify']);
  grunt.registerTask('css-task', ['stylus','cssmin']);
  // grunt.registerTask('stylus-task', ['stylus']);
};